package com.assignment.map;

import androidx.fragment.app.testing.FragmentScenario;
import androidx.navigation.Navigation;
import androidx.navigation.testing.TestNavHostController;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.runner.AndroidJUnit4;

import com.assignment.map.ui.VehiclesFragment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static org.junit.Assert.assertEquals;


@RunWith(AndroidJUnit4.class)
public class NavigationTest {
    private TestNavHostController navController;

    @Before
    public void createTestNavHostController() {
        // Create a TestNavHostController
        navController = new TestNavHostController(
                ApplicationProvider.getApplicationContext());
        navController.setGraph(R.navigation.navigation);
    }

    @Test
    public void testNavigation_Between_VehiclesFragment_And_MapFragment() {
        // Create a graphical FragmentScenario
        FragmentScenario<VehiclesFragment> vehiclesFragmentScenario = FragmentScenario.launchInContainer(VehiclesFragment.class);

        // Set the NavController property on the fragment
        vehiclesFragmentScenario.onFragment(fragment ->
                Navigation.setViewNavController(fragment.requireView(), navController)
        );

        //Navigate to VehicleFragment
        navController.navigate(R.id.action_mapFragment_to_vehiclesFragment);

        //Check that navigation is done successfully
        assertEquals(navController.getCurrentDestination().getId(), R.id.vehiclesFragment);

        //Navigate to MapFragment
        navController.navigate(R.id.action_vehiclesFragment_to_mapFragment);

        //Check that navigation is done successfully
        assertEquals(navController.getCurrentDestination().getId(), R.id.mapFragment);
    }

    @Test
    public void testNavigation_FromVehicleFragment_ToMapFragment_ByButtonClick() {
        // Create a graphical FragmentScenario
        FragmentScenario<VehiclesFragment> vehiclesFragmentScenario = FragmentScenario.launchInContainer(VehiclesFragment.class);

        // Set the NavController property on the fragment
        vehiclesFragmentScenario.onFragment(fragment ->
                Navigation.setViewNavController(fragment.requireView(), navController)
        );

        //Navigate to VehicleFragment
        navController.navigate(R.id.action_mapFragment_to_vehiclesFragment);

        //Check that navigation is done successfully
        assertEquals(navController.getCurrentDestination().getId(), R.id.vehiclesFragment);

        //Check that Button click navigates to MapFragment
        onView(ViewMatchers.withId(R.id.button)).perform(ViewActions.click());
        assertEquals(navController.getCurrentDestination().getId(), R.id.mapFragment);
    }
}
