package com.assignment.map.repository;

import com.assignment.map.data.network.Api;
import com.assignment.map.data.network.VehiclesDto;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Response;

public class VehicleRepositoryImp implements VehicleRepository{
    private Api api;

    @Inject
    public VehicleRepositoryImp(Api api) {
        this.api = api;
    }

    public Single<Response<VehiclesDto>> getVehiclesFromApi() {
        return api.getVehicleList();
    }
}
