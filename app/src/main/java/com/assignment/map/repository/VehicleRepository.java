package com.assignment.map.repository;

import com.assignment.map.data.network.VehiclesDto;

import io.reactivex.Single;
import retrofit2.Response;

public interface VehicleRepository {
    Single<Response<VehiclesDto>> getVehiclesFromApi();
}
