package com.assignment.map.di;

import android.content.Context;

import com.assignment.map.data.db.AppDatabase;
import com.assignment.map.data.db.VehicleDao;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    @Provides
    @Singleton
    public static AppDatabase provideAppDatabase(@Named("application_context") Context context) {
        return AppDatabase.getDatabase(context);
    }

    @Provides
    @Singleton
    public static VehicleDao provideVehicleDao(AppDatabase appDatabase) {
        return appDatabase.getVehicleDao();
    }
}
