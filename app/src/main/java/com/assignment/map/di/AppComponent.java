package com.assignment.map.di;

import android.content.Context;

import com.assignment.map.ui.MapViewModel;
import com.assignment.map.ui.VehiclesViewModel;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {RoomModule.class, ConnectionModule.class, VehicleModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder context(@Named("application_context") Context context);

        AppComponent build();

    }

    void inject(VehiclesViewModel vehiclesViewModel);

    void inject(MapViewModel mapViewModel);
}
