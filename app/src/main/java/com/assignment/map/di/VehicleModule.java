package com.assignment.map.di;

import com.assignment.map.repository.VehicleRepository;
import com.assignment.map.repository.VehicleRepositoryImp;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class VehicleModule {
    @Binds
    public abstract VehicleRepository bindVehicleRepository(VehicleRepositoryImp vehicleRepositoryImp);
}
