package com.assignment.map.data.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface VehicleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertVehicles(List<Vehicle> vehicle);

    @Query("SELECT * FROM vehicle")
    Flowable<List<Vehicle>> getAllVehicles();

    @Query("DELETE FROM vehicle")
    void deleteAllVehicles();
}
