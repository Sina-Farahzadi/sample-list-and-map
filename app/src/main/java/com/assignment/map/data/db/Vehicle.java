package com.assignment.map.data.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;

@Data
@Entity(tableName = "vehicle")
public class Vehicle {

    public Vehicle(String type, double latitude, double longitude, int bearing, String imageUrl) {
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.bearing = bearing;
        this.imageUrl = imageUrl;
    }

    @PrimaryKey(autoGenerate = true)
    private long id;

    private String type;

    private double latitude;

    private double longitude;

    private int bearing;

    @ColumnInfo(name = "image_url")
    private String imageUrl;
}