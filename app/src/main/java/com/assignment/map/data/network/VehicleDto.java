package com.assignment.map.data.network;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class VehicleDto {

    @SerializedName("type")
    private String type;

    @SerializedName("lat")
    private Double latitude;

    @SerializedName("lng")
    private Double longitude;

    @SerializedName("bearing")
    private Integer bearing;

    @SerializedName("image_url")
    private String imageUrl;
}
