package com.assignment.map.data.network;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;

public interface Api {
    @GET("assets/test/document.json")
    Single<Response<VehiclesDto>> getVehicleList();
}
