package com.assignment.map.data.network;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class VehiclesDto {

    @SerializedName("vehicles")
    private List<VehicleDto> vehiclesDto;
}
