package com.assignment.map;

import android.app.Application;

import com.assignment.map.di.AppComponent;
import com.assignment.map.di.DaggerAppComponent;

public class App extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .context(this)
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
