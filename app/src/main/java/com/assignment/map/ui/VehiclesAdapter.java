package com.assignment.map.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.assignment.map.R;
import com.assignment.map.data.db.Vehicle;
import com.assignment.map.databinding.VehicleItemBinding;
import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

public class VehiclesAdapter extends ListAdapter<Vehicle, VehiclesAdapter.ViewHolder> {

    private Context context;

    public VehiclesAdapter() {
        super(DIFF_CALLBACK);
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater mInflater = LayoutInflater.from(context);
        VehicleItemBinding binding = VehicleItemBinding.inflate(mInflater, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Vehicle vehicle = getItem(position);
        holder.binding.setVehicle(vehicle);
        Glide.with(context).load(vehicle.getImageUrl()).timeout(5000).placeholder(R.drawable.car).dontAnimate().into(holder.binding.imageView);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        VehicleItemBinding binding;

        ViewHolder(VehicleItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static final DiffUtil.ItemCallback<Vehicle> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Vehicle>() {
                @Override
                public boolean areItemsTheSame(@NonNull Vehicle oldVehicle, @NonNull Vehicle newVehicle) {
                    return oldVehicle.getId() == newVehicle.getId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull Vehicle oldVehicle, @NonNull Vehicle newVehicle) {
                    return oldVehicle.equals(newVehicle);
                }
            };
}
