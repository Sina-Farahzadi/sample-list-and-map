package com.assignment.map.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.assignment.map.R;
import com.assignment.map.data.db.Vehicle;
import com.assignment.map.databinding.FragmentMapBinding;
import com.assignment.map.util.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MapFragment extends Fragment implements OnMapReadyCallback {
    private final String TAG = getClass().getSimpleName();
    private FragmentMapBinding binding;
    private MapViewModel viewModel;
    private GoogleMap googleMap;
    private SupportMapFragment supportMapFragment;
    private Disposable disposable;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMapBinding.inflate(inflater, container, false);
        viewModel = ViewModelProviders.of(this).get(MapViewModel.class);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
    }

    private void observe() {
        //Glide getting the picture for calls need to be called in background.
        //Updating the map and animating it needs to be run on UI thread.
        //It seems car icons are needed to be in the resources to handle this in a more concise way.
        viewModel.getVehicleMutableLiveData().observe(getViewLifecycleOwner(), vehicles ->
                disposable = Observable.fromCallable(() -> {
                    showCarsOnMap(vehicles);
                    return true;
                }).observeOn(Schedulers.io())
                        .subscribeOn(Schedulers.io())
                        .subscribe(isComplete -> {
                                    //action is successful
                                }
                                , error -> Log.d(TAG, error.getMessage())
                        ));

        //Observing events
        viewModel.getEvents().observe(getViewLifecycleOwner(), event -> {
            switch (event) {
                case MapViewModel.NETWORK_ERROR:
                    Navigation.findNavController(binding.getRoot()).navigate(R.id.action_mapFragment_to_vehiclesFragment);
                    break;
                case MapViewModel.LOADING:
                    showLoading();
                    break;
                case MapViewModel.STOP_LOADING:
                    stopLoading();
                    break;
                default:
            }
        });
    }

    private void showLoading() {
        binding.loading.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        binding.loading.setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        observe();
        viewModel.getVehiclesFromApi();
    }


    /**
     * Due to loading icons with glide this method is calling from background.
     * Codes related to updating the map need to run on UI thread.
     * It seems car icons are needed to be in the resources to handle this in a more concise way.
     *
     * @param vehicles: list of vehicles to show on map.
     */
    private void showCarsOnMap(List<Vehicle> vehicles) {
        //TODO: Getting icons from url and adding the as marker icons led to an error prone design. Change is needed.
        List<Marker> markers = new ArrayList<>();
        Context context = requireContext();
        for (Vehicle vehicle : vehicles) {
            LatLng latLng = new LatLng(vehicle.getLatitude(), vehicle.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);

            //Based on glide documentations, I think it is advisable to load icons from resources.
            FutureTarget<Bitmap> futureTarget =
                    Glide.with(context)
                            .asBitmap()
                            .load(vehicle.getImageUrl()).timeout(5000) //design is error prone because of loading icons is async
                            .submit(AppUtils.dpToPixel(context, 50), AppUtils.dpToPixel(context, 50));

            try {
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(futureTarget.get()));
            } catch (ExecutionException e) {
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                e.printStackTrace();
            } catch (InterruptedException e) {
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                e.printStackTrace();
            }

            requireActivity().runOnUiThread(() -> markers.add(googleMap.addMarker(markerOptions)));
        }

        requireActivity().runOnUiThread(() -> {
            //Locate Bounds and animate camera
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markers) {
                builder.include(marker.getPosition());
            }
            if (markers.size() > 0) {
                LatLngBounds bounds = builder.build();
                int padding = AppUtils.calculateMapPadding(context);
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                requireActivity().runOnUiThread(() -> googleMap.animateCamera(cu));
            }
        });

        viewModel.setEvents(MapViewModel.STOP_LOADING);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (disposable != null) {
            disposable.dispose();
        }
    }
}
