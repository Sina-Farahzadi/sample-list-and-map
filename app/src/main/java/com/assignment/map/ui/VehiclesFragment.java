package com.assignment.map.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.assignment.map.R;
import com.assignment.map.databinding.FragmentVehiclesBinding;

public class VehiclesFragment extends Fragment implements View.OnClickListener {
    private FragmentVehiclesBinding binding;
    private VehiclesViewModel viewModel;
    private VehiclesAdapter adapter = new VehiclesAdapter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentVehiclesBinding.inflate(inflater, container, false);
        viewModel = ViewModelProviders.of(this).get(VehiclesViewModel.class);
        binding.setListener(this);
        binding.vehiclesRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false));
        binding.vehiclesRecyclerView.setAdapter(adapter);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        observe();
        viewModel.getAllVehiclesFromDatabase();

        //Closing app with BackPressed
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                requireActivity().finish();
            }
        });
    }

    private void observe() {
        viewModel.getVehicleMutableLiveData().observe(getViewLifecycleOwner(), vehicles -> adapter.submitList(vehicles));

        viewModel.getEvents().observe(getViewLifecycleOwner(), events -> {
            if (events.equals(VehiclesViewModel.DATABASE_ERROR)) {
                Toast.makeText(requireContext(), "An Error Occurred.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button) {
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_vehiclesFragment_to_mapFragment);
        }
    }
}
