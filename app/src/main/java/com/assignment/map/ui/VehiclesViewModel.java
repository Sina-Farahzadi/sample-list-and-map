package com.assignment.map.ui;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.assignment.map.App;
import com.assignment.map.data.db.Vehicle;
import com.assignment.map.data.db.VehicleDao;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class VehiclesViewModel extends AndroidViewModel {
    public static final String DATABASE_ERROR = "database_error";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    VehicleDao vehicleDao;

    private MutableLiveData<String> events = new MutableLiveData<>();
    private MutableLiveData<List<Vehicle>> vehicleMutableLiveData = new MutableLiveData<>();

    public VehiclesViewModel(@NonNull Application application) {
        super(application);
        ((App) getApplication()).getAppComponent().inject(this);
    }


    public void getAllVehiclesFromDatabase() {
        compositeDisposable.add(vehicleDao.getAllVehicles()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(vehicles -> {
                    vehicleMutableLiveData.postValue(vehicles);
                }, error -> {
                    events.postValue(DATABASE_ERROR);
                }, () -> {
                }));
    }

    public MutableLiveData<List<Vehicle>> getVehicleMutableLiveData() {
        return vehicleMutableLiveData;
    }

    public MutableLiveData<String> getEvents() {
        return events;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}