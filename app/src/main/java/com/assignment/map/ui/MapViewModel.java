package com.assignment.map.ui;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.assignment.map.App;
import com.assignment.map.data.db.Vehicle;
import com.assignment.map.data.db.VehicleDao;
import com.assignment.map.data.network.VehicleDto;
import com.assignment.map.repository.VehicleRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MapViewModel extends AndroidViewModel {
    final String TAG = getClass().getSimpleName();

    public static final String NETWORK_ERROR = "network_error";
    public static final String LOADING = "loading";
    public static final String STOP_LOADING = "stop_loading";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    VehicleRepository vehicleRepository;
    @Inject
    VehicleDao vehicleDao;

    private MutableLiveData<String> events = new MutableLiveData<>();
    private MutableLiveData<List<Vehicle>> vehicleMutableLiveData = new MutableLiveData<>();

    public MapViewModel(@NonNull Application application) {
        super(application);
        ((App) getApplication()).getAppComponent().inject(this);
    }

    public void getVehiclesFromApi() {
        events.setValue(LOADING);
        compositeDisposable.add(vehicleRepository.getVehiclesFromApi()
                .subscribeOn(Schedulers.io())
                .subscribe(vehicles -> {
                    if (vehicles.body() != null) {
                        vehicleMutableLiveData.postValue(DtoToEntity(vehicles.body().getVehiclesDto()));
                        compositeDisposable.add(Completable.fromCallable((Callable<Void>) () -> { //Delete as a completable and chain it to the insert
                            vehicleDao.deleteAllVehicles(); //delete from database
                            return null;
                        }).andThen(vehicleDao.insertVehicles(DtoToEntity(vehicles.body().getVehiclesDto()))).subscribe(() -> Log.d(TAG, "Data inserted"))); //Subscribes on Schedulers.io()
                    }
                }, error -> events.postValue(NETWORK_ERROR)));
    }

    /**
     * Object Mapper
     *
     * @param vehicleDtos a List of VehicleDto
     * @return A list of Vehicle
     */
    private List<Vehicle> DtoToEntity(List<VehicleDto> vehicleDtos) {
        List<Vehicle> vehicles = new ArrayList<>();

        for (VehicleDto vehicleDto : vehicleDtos) {
            Vehicle vehicle = new Vehicle(vehicleDto.getType(),
                    vehicleDto.getLatitude(),
                    vehicleDto.getLongitude(),
                    vehicleDto.getBearing(),
                    vehicleDto.getImageUrl()
            );
            vehicles.add(vehicle);
        }
        return vehicles;
    }

    public void setEvents(String event) {
        events.postValue(event);
    }

    public MutableLiveData<List<Vehicle>> getVehicleMutableLiveData() {
        return vehicleMutableLiveData;
    }

    public MutableLiveData<String> getEvents() {
        return events;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}