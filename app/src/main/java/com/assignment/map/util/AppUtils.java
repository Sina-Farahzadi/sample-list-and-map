package com.assignment.map.util;

import android.content.Context;

public class AppUtils {
    /**
     * @return calculates the padding for the map
     */
    public static int calculateMapPadding(Context context) {
        int widthDp = context.getResources().getConfiguration().screenWidthDp;
        return widthDp / 10;
    }

    public static int dpToPixel(final Context context, final float dp) {
        return Math.round(dp * context.getResources().getDisplayMetrics().density);
    }
}
