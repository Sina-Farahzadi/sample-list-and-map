package com.assignment.map;

import com.assignment.map.data.network.Api;
import com.assignment.map.repository.VehicleRepositoryImp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RepositoryTest {

    @Mock
    private Api api;

    private VehicleRepositoryImp repositoryImp;

    @Before
    public void setup() {
        repositoryImp = new VehicleRepositoryImp(api);
    }

    //Checking if calling the correct method from Api
    @Test
    public void callingRepositoryMethod_Calls_ApiMethod() {
        repositoryImp.getVehiclesFromApi();
        verify(api).getVehicleList();
    }
}